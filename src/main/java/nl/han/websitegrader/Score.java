package nl.han.websitegrader;

enum Aspect {
    BP1_KNOCKOUT,
    BP1_BESTANDEN,
    BP1_UITWERKING,
}

class Score {
    final Aspect aspect;
    final String comment;
    final String url;

    Score(final Aspect aspect, final String comment, final String url) {
        this.aspect = aspect;
        this.comment = comment.strip();
        this.url = url.strip();
    }
}
