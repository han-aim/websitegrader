package nl.han.websitegrader;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONException;
import kong.unirest.json.JSONObject;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

class Websitegrader implements AutoCloseable {
    private static final Dimension[] N_BREAKPOINT = {
            new Dimension(360, 640),
            new Dimension(1366, 768),
            new Dimension(1920, 1080),
            new Dimension(375, 667),
            new Dimension(360, 720),
            new Dimension(768, 1024)
    };
    private final HashSet<String> nUrlPathProcessed = new HashSet<>();
    private final WebDriver driver;
    private final URI urlBase;
    private final Path directoryReport;
    private final ArrayList<Score> grading = new ArrayList<>();
    private final Queue<URI> nUrlOutgoing = new LinkedBlockingQueue<>();

    Websitegrader(final String urlBaseString) throws URISyntaxException, IOException {
        final ChromeOptions options = new ChromeOptions();
        options.setCapability(ChromeOptions.CAPABILITY, getChromeOptions());
        driver = new ChromeDriver(options);
        urlBase = new URI(urlBaseString);
        directoryReport = Files.createTempDirectory(Paths.get("."), "websitegrader_");
    }

    // Based on: https://stackoverflow.com/a/54570357
    private ChromeOptions getChromeOptions() {
        final ChromeOptions chromeOptions = new ChromeOptions();
        final LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.PERFORMANCE, java.util.logging.Level.OFF);
        logPrefs.enable(LogType.PROFILER, java.util.logging.Level.OFF);
        logPrefs.enable(LogType.BROWSER, java.util.logging.Level.ALL);
        logPrefs.enable(LogType.CLIENT, java.util.logging.Level.ALL);
        logPrefs.enable(LogType.DRIVER, java.util.logging.Level.WARNING);
        logPrefs.enable(LogType.SERVER, java.util.logging.Level.ALL);
        chromeOptions.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        // todo Headless mode does not seem to work.
        chromeOptions.setHeadless(true);
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--disable-extensions");
        chromeOptions.addArguments("--proxy-server='direct://'");
        chromeOptions.addArguments("--proxy-bypass-list=*");
        return chromeOptions;
    }

    // Checks using Content-Security-Policy.
    private void checkUsingWebdriver(URI url) throws IOException {
        driver.manage().window().maximize();
        driver.get(url.toString());
        for (final Dimension dimension : N_BREAKPOINT) {
            driver.manage().window().setSize(dimension);
            final File fileScreenshotTemp = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            // Now you can do whatever you need to do with it, for example copy somewhere
            final String namePage = URLEncoder.encode(url.getPath(), StandardCharsets.US_ASCII);
            final String filenameScreenshot = String.format("Screen_%s_%dx%dpx.png", namePage, dimension.width, dimension.height);
            final Path pathScreenshot = directoryReport.resolve(filenameScreenshot);
            try {
                // todo handle FileAlreadyExistsException
                Files.copy(fileScreenshotTemp.toPath(), pathScreenshot);
            } catch (final IOException error) {
                Main.logger.fatal(String.format("Failed to save screenshot: %s", error));
                throw error;
            }
        }
        final LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
        for (final LogEntry entry : logEntries) {
            if (entry.getLevel() == java.util.logging.Level.SEVERE) {
                final String message = entry.getMessage();
                final String messageNew;
                final Aspect aspect;
                // todo Improve matching
                final String[] messageSplit = message.split("\\s+", 2);
                if (message.contains("javascript:") || message.contains("inline script")) {
                    // JavaScript related error.
                    aspect = Aspect.BP1_KNOCKOUT;
                    messageNew = "Is using JavaScript. ";
                } else if (message.contains("inline style") || message.contains("CSS")) {
                    // CSS related error.
                    aspect = Aspect.BP1_BESTANDEN;
                    messageNew = "Is using inline CSS (<style> element or attribute). ";
                } else if (message.contains("404 (Not Found)")) {
                    // Dead link.
                    messageNew = "Contains a dead link. ";
                    aspect = Aspect.BP1_BESTANDEN;
                } else {
                    messageNew = "The browser encountered a generic issue. You may want to look into it. Message: " + messageSplit[1];
                    aspect = Aspect.BP1_UITWERKING;
                }
                final Score score = new Score(aspect, messageNew, messageSplit[0]);
                grading.add(score);
            }
        }
    }

    // Check whether URL path is lowercase and does not contain URL-encoded characters (e.g., spaces).
    private void checkPath(final String rawPath, final String url) {
        try {
            final String path = URLDecoder.decode(rawPath, StandardCharsets.UTF_8.toString());
            if (!path.equals(rawPath)) {
                final Score score = new Score(Aspect.BP1_BESTANDEN, String.format("URL path '%s' contains URL-encoded characters", rawPath), url);
                grading.add(score);
            }
            if (!path.equals(path.toLowerCase())) {
                final Score score = new Score(Aspect.BP1_BESTANDEN, String.format("URL path '%s' isn't lowercase", path), url);
                grading.add(score);
            }
        } catch (UnsupportedEncodingException error) {
            final Score score = new Score(Aspect.BP1_BESTANDEN, String.format("URL path '%s' cannot be decoded: %s", rawPath, error), url);
            grading.add(score);
        }
    }

    // Returns outgoing URL, if valid.
    private void checkOutgoingUrl(final String urlOutgoingPerhaps, final URI urlOrigin) {
        try {
            URI urlOutgoing = new URI(urlOutgoingPerhaps);
            // Check for non-relative URLs
            if (urlOutgoing.isAbsolute()) {
                final Score score = new Score(Aspect.BP1_BESTANDEN, String.format("Absolute URL %s", urlOutgoing), urlOrigin.toString());
                grading.add(score);
            } else {
                urlOutgoing = urlBase.resolve(urlOutgoing);
                final String pathUrlOutgoingNew = urlOutgoing.getPath();
                if (!urlOutgoing.equals(urlOrigin) && pathUrlOutgoingNew != null && !pathUrlOutgoingNew.isEmpty()) {
                    Main.logger.trace("Enqueueing URL {}", urlOutgoing);
                    nUrlOutgoing.add(urlOutgoing);
                }
            }
        } catch (final URISyntaxException error) {
            final Score score = new Score(Aspect.BP1_BESTANDEN, String.format("Invalid URL '%s': %s", urlOutgoingPerhaps, error), urlOrigin.toString());
            grading.add(score);
        }
    }

    void checkUsingNuValidator(final URI urlOrigin) {
        final Map<String, Object> queryConf = new HashMap<>();
        queryConf.put("doc", urlOrigin.toString());
        queryConf.put("out", "json");
        final HttpResponse<JsonNode> uniResponse = Unirest.get("http://localhost:8888/")
                .queryString(queryConf)
                .asJson();
        final JSONArray nMessage = uniResponse.getBody().getObject().getJSONArray("messages");
        for (int index = 0; index < nMessage.length(); index++) {
            // If no error was found, the output is the empty string.
            final JSONObject message = nMessage.getJSONObject(index);
            final Score score;
            int lastLine = -1;
            int lastColumn = -1;
            int firstLine = -1;
            int firstColumn = -1;
            try {
                lastLine = message.getInt("lastLine");
                lastColumn = message.getInt("lastColumn");
                firstLine = message.getInt("firstLine");
                firstColumn = message.getInt("firstColumn");
            } catch (final JSONException ignored) {
            }
            final String location = urlOrigin.toString() + " lines " + firstLine + " to " + lastLine + " letters " + firstColumn + " to " + lastColumn;
            if (message.getString("type").equals("error")) {
                score = new Score(Aspect.BP1_KNOCKOUT, message.getString("message"), location);
            } else {
                score = new Score(Aspect.BP1_UITWERKING, message.getString("message"), location);
            }
            grading.add(score);
        }
    }

    void checkWebsiteStructure() {
        Main.logger.info("Checking website structure");
        final Set<Path> pathsProcessed = this.nUrlPathProcessed.stream().map(Paths::get).collect(Collectors.toUnmodifiableSet());
        final Map<String, Set<Path>> contenttypeToNPathContaining = new HashMap<>();
        for (final Path path : pathsProcessed) {
            final Path fileName = path.getFileName();
            if (fileName != null) {
                final String contenttype = URLConnection.guessContentTypeFromName(fileName.toString());
                // Add the containing directory to a set of paths for each content type.
                if (contenttype != null) {
                    contenttypeToNPathContaining.computeIfAbsent(contenttype, key -> new HashSet<>()).add(path.getParent());
                }
            }
        }
        for (final Map.Entry<String, Set<Path>> entry : contenttypeToNPathContaining.entrySet()) {
            // If multiple containing directories (parent paths) were recorded, not all content of that content type is in the same directory.
            final Set<Path> nPathContaining = contenttypeToNPathContaining.get(entry.getKey());
            final boolean sameDirectory = nPathContaining.size() == 1;
            if (!sameDirectory) {
                final String message = String.format("More than one containing directory (%s) for content of type %s. ", nPathContaining, entry.getKey());
                final Score score = new Score(Aspect.BP1_BESTANDEN, message, urlBase.toString());
                grading.add(score);
            }
        }
    }

    void check() throws IOException {
        Main.logger.info("Checking start document at URL {}", urlBase);
        nUrlOutgoing.add(urlBase);
        while (!nUrlOutgoing.isEmpty()) {
            // todo Use Internet Media type
            final URI urlOutgoing = nUrlOutgoing.remove();
            final String pathUrlOutgoing = urlOutgoing.getPath();
            if (!nUrlPathProcessed.contains(pathUrlOutgoing)) {
                Main.logger.info("Processing URL {}", urlOutgoing);
                checkPath(urlOutgoing.getRawPath(), urlOutgoing.toString());
                // todo Does this accept fragment IDs?
                if (pathUrlOutgoing.equals("") || pathUrlOutgoing.equals("/") || pathUrlOutgoing.endsWith(".html")) {
                    try {
                        final Document document = Jsoup.parse(urlOutgoing.toURL(), 1000);
                        Main.logger.debug("Checking document {}", urlOutgoing);
                        // Select all elements with attributes containing an outgoing URL.
                        final Elements nLink = document.select("*[href], *[src]");
                        for (final Element link : nLink) {
                            for (final Attribute attribute : link.attributes()) {
                                final String key = attribute.getKey();
                                if (key.equals("href") || key.equals("src")) {
                                    checkOutgoingUrl(attribute.getValue(), urlOutgoing);
                                }
                            }
                        }
                        checkUsingWebdriver(urlOutgoing);
                        checkUsingNuValidator(urlOutgoing);
                    } catch (final IOException error) {
                        Main.logger.error("Failed to HTTP GET URL {}", urlOutgoing);
                    }
                } else if (pathUrlOutgoing.endsWith(".css")) {
                    checkUsingNuValidator(urlOutgoing);
                } else {
                    Main.logger.debug("Terminal URL {}", urlOutgoing);
                }
                Main.logger.info("Processed URL {}", urlOutgoing);
                nUrlPathProcessed.add(urlOutgoing.getPath());
            }
        }
        checkWebsiteStructure();
        writeReport();
    }

    private void writeReport() throws IOException {
        grading.sort(new ScoreComparator());
        try (final Workbook workbook = new XSSFWorkbook()) {
            try (final OutputStream outputStream = Files.newOutputStream(directoryReport.resolve("grading_auto.xlsx"))) {
                final Sheet sheet = workbook.createSheet("Grading");
                int indexRow = 0;
                final Row rowHeader = sheet.createRow(indexRow++);
                final Cell cellAspectHeader = rowHeader.createCell(0);
                cellAspectHeader.setCellValue("Issue for aspect");
                final Cell cellCommentHeader = rowHeader.createCell(1);
                cellCommentHeader.setCellValue("Feedback");
                final Cell cellUrlHeader = rowHeader.createCell(2);
                cellUrlHeader.setCellValue("Location");
                for (final Score score : grading) {
                    final Row row = sheet.createRow(indexRow++);
                    final Cell cellAspect = row.createCell(0);
                    cellAspect.setCellValue(score.aspect.toString());
                    final Cell cellComment = row.createCell(1);
                    cellComment.setCellValue(score.comment);
                    final Cell cellUrl = row.createCell(2);
                    cellUrl.setCellValue(score.url);
                }
                for (int indexColumn = 0; indexColumn < sheet.getRow(0).getPhysicalNumberOfCells(); indexColumn++) {
                    sheet.autoSizeColumn(indexColumn);
                }
                workbook.write(outputStream);
            }
        }
    }

    @Override
    public void close() {
        driver.quit();
    }
}
