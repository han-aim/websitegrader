package nl.han.websitegrader;

import javax.annotation.Nonnull;
import java.util.Comparator;

class ScoreComparator implements Comparator<Score> {
    @Override
    public int compare(final @Nonnull Score a, @Nonnull final Score b) {
        int cmp = a.aspect.compareTo(b.aspect);
        if (cmp == 0)
            cmp = a.url.compareTo(b.url);
        return cmp;
    }
}
