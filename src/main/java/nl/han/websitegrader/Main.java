package nl.han.websitegrader;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.*;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {
    static Logger logger;

    private static Logger initLogger() {
        final String filenameLog = "websitegrader.log";
        final ConfigurationBuilder<BuiltConfiguration> builder
                = ConfigurationBuilderFactory.newConfigurationBuilder();
        final AppenderComponentBuilder console
                = builder.newAppender("stdout", "Console");
        builder.add(console);
        final AppenderComponentBuilder file
                = builder.newAppender("file", "File");
        file.addAttribute("fileName", filenameLog);
        builder.add(file);
        final FilterComponentBuilder flow = builder.newFilter(
                "MarkerFilter",
                Filter.Result.ACCEPT,
                Filter.Result.DENY);
        flow.addAttribute("marker", "FLOW");
        console.add(flow);
        final LayoutComponentBuilder standard
                = builder.newLayout("PatternLayout");
        standard.addAttribute("pattern", "%d [%t] %-5level: %msg%n%throwable");
        console.add(standard);
        file.add(standard);
        final RootLoggerComponentBuilder rootLogger
                = builder.newRootLogger(Level.DEBUG);
        rootLogger.add(builder.newAppenderRef("stdout"));
        rootLogger.add(builder.newAppenderRef("file"));
        builder.add(rootLogger);
        final LoggerContext ctx = Configurator.initialize(builder.build());
        ctx.updateLoggers();
        return LogManager.getLogger();
    }

    public static void main(String[] args) throws URISyntaxException, IOException {
        // Disable the Nu Validator Main class from forcing an exit, so that resources will be cleaned up normally.
        logger = initLogger();
        logger.info("Started");
        try (final Websitegrader websitegrader = new Websitegrader("http://localhost/")) {
            websitegrader.check();
        }
        logger.info("Exiting");
        LogManager.shutdown();
    }
}
