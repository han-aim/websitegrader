# Websitegrader

Dit is een software-oplossing die het beoordelingsproces betrouwbaarder en sneller maakt voor de beroepsproducten van de course Web Technology van HBO-ICT, ICA, HAN University of Applied Sciences.

# Definities

# Subresource

Een deel van de websitegrader, zoals HTML-pagina's, stylesheets, media, evt. fonts, etc.

# Doelstellingen

## Voor de huidige uitvoering

* De beoordeling t.a.v. de knock-out criteria grondiger en betrouwbaarder (eerlijker) uitvoeren dan praktisch haalbaar is met de hand.
* De objectieve, technische vereisten veel sneller, reproduceerbaarder/aanwijsbaarder kunnen checken dan met de hand mogelijk is.

## Voor volgende uitvoeringen

* Websitegrader ook didactisch inzetten, bijv. voor feedback en self-assessment.
* De puntentelling (deels) automatiseren.

# Installatie

De volgende instructies gelden voor macOS. Als het goed is zijn de instructies zonder onvoorspelbare aanpassingen te volgen onder andere besturingssystemen.

## Afhankelijkheden

* Installeer Java 13.
* Installeer een Chromium-achtige browser (Chrome, Brave, etc.).
* Installeer de juiste [Chrome WebDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads).
  * Mocht je onder macOS Catalina werken, dan kan je [om de vereiste dat de WebDriver executable notarized is heen werken](https://firefox-source-docs.mozilla.org/testing/geckodriver/Notarization.html).
* Download de [Nu Validator](https://github.com/validator/validator/releases/tag/18.11.5). Pak het ZIP-bestand ergens uit.

## Bouwen (voor ontwikkelaars)

* Ga naar de map waarin dit softwareproject staat.
* Bouw websitegrader met Maven:
```sh
mvn package
```
* In de map `target/` vind je nu `websitegrader-1.0-SNAPSHOT.jar`.

## Starten

* Start de Nu Validator (vanuit de map waarin `vnu.jar` staat) als lokale webservice:
```sh
java -Dnu.validator.servlet.bind-address=127.0.0.1 -cp vnu.jar nu.validator.servlet.Main 8888
```

* Start websitegrader (vanuit de map waarin `websitegrader-1.0.-SNAPSHOT.jar` staat):
```sh
java -Dwebdriver.chrome.driver="$PATH_DRIVER_EXECUTABLE" -jar websitegrader-1.0-SNAPSHOT.jar
```

waar `$PATH_DRIVER_EXECUTABLE` het absolute pad naar de executable van de Chrome WebDriver is.

# Use case: beoordeling beroepsproduct

## Zet het te beoordelen beroepsproduct klaar

Extraheer de websitegrader in de `htdocs`-map (document root) van de Apache HTTP Webserver uit XMAPP. Wis eerst eventuele overige content daarin.

In de `httpd.conf` van Apache Web server van XAMPP staat voor macOS standaard het volgende:

    DocumentRoot "/Applications/XAMPP/xamppfiles/htdocs"
    <Directory "/Applications/XAMPP/xamppfiles/htdocs">
    
Ik raad aan deze parameters telkens zo te veranderen zodat dat ieder te beoordelen beroepsproduct in een aparte map zit en blijft zitten. 

## Stel de Apache HTTP Server strakker in

Plaats [`.htaccess`](./.htaccess) in de `htdocs`-map

Dit zorgt ervoor dat Apache HTTP Server bepaalde headers instelt die het beoordelen veiliger en geautomatiseerd maakt.

# Beoordelingsmodel beroepsproduct 1: statische HTML/CSS websitegrader

**Legenda**

* ✅ Gerealiseerd.
* 🚧 Realisatie waarschijnlijk/bezig.
* ❌ Buiten scope.

## Knock-out

> Geen formeel overleg met de senior (docent)

❌

> HTML pagina valideert niet (validator.w3.org).

✅ Websitegrader valideert alle vanuit de index bereikbare HTML-pagina's met de Nu Validator.

> Gebruik gemaakt van framework, library of JavaScript
> Website draait niet zonder internetverbinding (externe afbeeldingen)

🚧 Gebruik gemaakt van framework, library.

✅ D.m.v. de [Selenium WebDriver](https://selenium.dev/projects/) en `Content-Security-Policy` dwingt websitegrader af:
 * dat de student op geen enkele manier JavaScript gebruikt. (Dat is heel lastig uit te sluiten met de hand.).
 * dat geen subresources ingeladen worden buiten de XAMPP-webserver.
Schendingen van deze control worden door websitegrader gerapporteerd.

> Te weinig: niet over student zelf, te weinig pagina's inhoud

❌

## Bestanden

> `index.html` als homepage

✅ Websitegrader neemt als startpunt (base URL) de opgegeven URL. Standaard is dat `http://localhost/`. Dat wordt vertaald naar `index.html` door Apache webserver. Als dat een HTTP response met code 404 oplevert dan weet je dat niet voldaan is aan dit criterium. Dan moet voor verdere beoordeling wel een andere base worden opgegeven.

🚧 Websitegrader gebruikt niet de hardcoded extensies `.html` en `.css` tijdens het checken, maar het content type zoals teruggegeven door de webserver. 

> Correcte bestandsnamen en mapnamen (kleine letters, geen spaties, naam past bij inhoud)

❌ Naam past bij inhoud.

✅ Overige criteria. Websitegrader spoort niet alleen spaties op maar alle URL-encoded karakters (`%20` etc.) in URLs.

> Bestanden georganiseerd (de hele websitegrader staat binnen 1 websitemap, CSS, afbeeldingen in aparte submappen, geen 'vreemde' bestanden in de websitemap)

✅ D.m.v. XAMPP worden relatieve URLs buiten `htdocs/` sowieso niet gevolgd. Absolute URLs worden niet gevolgd.

✅ Websitegrader spoort inline CSS op.

✅ Websitegrader checkt of voor alle subresources van ieder content type geldt dat deze in één map per content type staan. Dat betekent o.a. dat CSS en HTML nooit in dezelfde map mogen staan. Alleen de echt gelinkte subresources worden meegenomen.

## Uitwerking

> Personage is duidelijk
> Inhoudelijk verzorgd
> Stilistisch verzorgd

❌

> 5 pagina's met passende inhoud
> Gebruik van stijlelementen (logo, kleuren, achtergronden, grafische elementen)
> Pagina lay-out
> Navigatie
> Formulier

🚧

## Responsiveness

> De inhoud past zich aan aan verschillende schermbreedtes
> Er zijn breakpoints gebruikt
> Ook het menu schaalt mee

✅ D.m.v. de Selenium genereert websitegrader screenshots van de viewport op verschillende breakpoints. Deze breakpoints zijn gebaseerd op [de best practice voor 2019 van BrowserStack](https://www.browserstack.com/guide/responsive-design-breakpoints).
 
🚧 D.m.v. [CSS Parser](http://cssparser.sourceforge.net/) checkt websitegrader of er ten minste 2 media queries zijn.

## HTML

> Structurering content (met HTML5 structuurelementen gewerkt, hiërarchische kopjes, geen overbodige div's)
> Menu (correcte structuur, ook zonder CSS, op alle pagina's, werkend)
> Correcte basisstructuur (`html`, `head`, `body`)
> Formulier (labels, juiste elementen, attributen, volledig)

🚧

> Relatieve url's naar pagina's en media

✅ Websitegrader check of alle URLs relatief zijn (onder criterium 'Bestanden').
